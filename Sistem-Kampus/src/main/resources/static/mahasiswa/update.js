$(document).ready(function(){
    GetMahasiswaById();
})

function GetMahasiswaById() {
    var id = $("#editMhsId").val();
    console.log(id);
    $.ajax({
        url: "/api/mahasiswa/" + id,
        type: "GET",
        contentType: "application/json",
        success: function(data) {
            $("#namaMhs").val(data.namaMhs);
            $("#jk").val(data.jk);
            $("#alamat").val(data.alamat);
        }
    });
}

$("#updateMhsBtnCancel").click(function(){
    $(".modal").modal("hide")
})

$("#updateMhsBtnCreate").click(function(){
    var id = $("#editMhsId").val();
    var namaMhs = $("#namaMhs").val();
    var jk = $("#jk").val();
    var alamat = $("#alamat").val();

    if(namaMhs == ""){
        $("#errNamaMhs").text("Nama Mahasiswa tidak boleh kosong!");
        return;
    } else {
        $("#errNamaMhs").text("");
    }
    if(jk == ""){
        $("#errJK").text("Jenis Kelamin tidak boleh kosong!");
        return;
    } else {
        $("#errJK").text("");
    }
    if(alamat == ""){
        $("#alamat").text("Alamat tidak boleh kosong!");
        return;
    } else {
        $("#errAlamat").text("");
    }

    var object = {}
    object.namaMhs = namaMhs;
    object.jk = jk;
    object.alamat = alamat;

    var myJson = JSON.stringify(object);

    $.ajax({
        url: "/api/mahasiswa/" + id,
        type: "PUT",
        contentType: "application/json",
        data: myJson,
        success: function(data){
            $(".modal").modal("hide")
            GetAllMahasiswaMapped(0, 5);
        },
        error: function() {
            alert("Terjadi Kesalahan")
        }
    });
})