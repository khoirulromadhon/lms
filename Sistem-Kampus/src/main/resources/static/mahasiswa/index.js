function GetAllMahasiswa(currentPage, length){
    console.log("current:" + currentPage);
    console.log("length:" + length);
    $("#mhsTable").html(
        `
        <thead>
            <tr>>
                <th>Nama Mahasiswa</th>
                <th>Jenis Kelamin</th>
                <th>Alamat</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody id="mhsTBody"></tbody>
        `
    );

    $.ajax({
        url: "api/mahasiswaMapped?page=" + currentPage + "&size=" + length,
        type: "GET",
        contentType: "application/json",
        success: function(data){
            for(i = 0; i < data.mahasiswas.length; i++){
                $("#mhsTBody").append(
                    `
                    <tr>
                        <td>${data.mahasiswas[i].namaMhs}</td>
                        <td>${data.mahasiswas[i].jk}</td>
                        <td>${data.mahasiswas[i].alamat}</td>
                        <td>
                            <button value="${data.mahasiswas[i].id}" onClick="editMhs(${data.mahasiswas[i].id})" class="btn btn-warning">
                                <i class="bi-pencil-square"></i>
                            </button>
                            <button value="${data.mahasiswas[i].id}" onClick="deleteMhs(${data.mahasiswas[i].id})" class="btn btn-danger">
                                <i class="bi-trash"></i>
                            </button>
                        </td>
                    </tr>
                    `
                )
            }
        }
    });

    $("#pagination").html(
        `
            <nav aria-label="Page navigation">
                <ul class="pagination">
                    <li class="page-item"><a class="page-link" onClick="GetAllMahasiswa(' + (data.currentPage - 1) + ',' + length + ')">Previous</a></li>
                    let index = 1;
                    for (let i = 0; i < data.totalPages; i++) {
                        <li class="page-item"><a id="pageslink" class="page-link" onclick="GetAllMahasiswaMapped(' + i + ',' + length + ')"> + index + </a></li>;
                        index++;
                    }
                    <li class="page-item"><a class="page-link" onClick="GetAllMahasiswa(' + (data.currentPage + 1) + ',' + length + ')">Next</a></li>
                </ul>
            </nav>
        `
    )
}

$("#addBtn").click(function(){
    $.ajax({
        url: "/mahasiswa/create",
        type: "GET",
        contentType: "html",
        success: function(data){
            $(".modal-title").text("Input Data Mahasiswa Baru");
            $(".modal-body").html(data);
            $(".modal").modal("show");
        }
    });
})

function editMhs(id){
    $.ajax({
        url: "/mahasiswa/update/" + id,
        type: "GET",
        contentType: "html",
        success: function(data){
            $(".modal-title").text("Update Data Mahasiswa");
            $(".modal-body").html(data);
            $(".modal").modal("show");
        }
    });
}

function deleteMhs(id){
    $.ajax({
        url: "/api/mahasiswa/" + id,
        type: "DELETE",
        success: function(data) {
            location.reload();
            alert("Berhasil Menghapus Data !!!!!");
        },
        error: function() {
            alert("Gagal Menghapus Data !!!!!");
        }
    });
}

function GetAllMahasiswaMapped(currentPage, length){
    $.ajax({
        url: "/api/mahasiswaMapped?page=" + currentPage + "&size" + length,
        type: "GET",
        contentType: "application/json",
        success: function(data){
            let table = '<select class="custom-select mt-3" id="size" onchange="GetAllMahasiswaMapped(0, 5)">'
            table += '<option value="3" selected>..</option>'
            table += '<option value="5">5</option>'
            table += '<option value="10">10</option>'
            table += '<option value="15">15</option>'
            table += '</select>'
            table += "<table class='table table-bordered mt-3'>";
            table += "<tr> <th width='10%' class='text-center'>No</th> <th>Nama Mahasiswa</th> <th>Jenis Kelamin</th> <th>Alamat</th>  <th>Action</th> </tr>"
            for (let i = 0; i < data.mahasiswas.length; i++){
                table += "<tr>";
                table += "<td class='text-center'>" + ((i + 1) + (data.currentPage * length)) + "</td>";
                table += "<td>" + data.mahasiswas[i].namaMhs + "</td>";
                table += "<td>" + data.mahasiswas[i].jk + "</td>";
                table += "<td>" + data.mahasiswas[i].alamat + "</td>";
                table += "<td><button class='btn btn-primary btn-sm' value='" + data.mahasiswas[i].id + "' onclick=editMhs(this.value)>Edit</button> <button class='btn btn-danger btn-sm' value='" + data.mahasiswas[i].id + "' onclick=deleteMhs(this.value)>Delete</button></td>";
                table += "</tr>";
            }
            table += "</table>";
            table += "<br>"
            table += '<nav aria-label="Page navigation">';
            table += '<ul class="pagination">'
            table += '<li class="page-item"><a class="page-link" onclick="GetAllMahasiswaMapped(' + (data.currentPage - 1) + ',' + length + ')">Previous</a></li>'
            let index = 1;
            for (let i = 0; i < data.totalPages; i++){
                table += '<li class="page-item"><a id="pageslink" class="page-link" onclick="GetAllMahasiswaMapped(' + i + ',' + length + ')">' + index + '</a></li>'
                index++;
            }
            table += '<li class="page-item"><a class="page-link" onclick="GetAllMahasiswaMapped(' + (data.currentPage + 1) + ',' + length + ')">Next</a></li>'
            table += '</ul>'
            table += '</nav>';
            $('#dataList').html(table);
        }
    });
}

function Search(request) {
    if(request.length > 0){
        $.ajax({
            url: "/api/searchMhs=" + request,
            type: "GET",
            contentType: "application/json",
            success: function (result){
                let table = "<table class='table table-bordered mt-3'>"
                table += "<tr> <th width='10%' class='text-center'>No</th> <th>Nama Mahasiswa</th> <th>Jenis Kelamin</th> <th>Alamat</th>  <th>Action</th> </tr>"
                if (result.length > 0){
                    for(let i = 0; i < result.length; i++){
                        table += "<tr>";
                        table += "<td class='text-center'>" + (i + 1) + "</td>";
                        table += "<td>" + result[i].namaMhs + "</td>";
                        table += "<td>" + result[i].jk + "</td>";
                        table += "<td>" + result[i].alamat + "</td>";
                        table += "<td><button class='btn btn-primary btn-sm' value='" + result[i].id + "' onclick=editMhs(this.value)>Edit</button> <button class='btn btn-danger btn-sm' value='" + result[i].id + "' onclick=deleteMhs(this.value)>Delete</button></td>";
                        table += "</tr>";
                    }
                }
                else {
                    table += "<tr>";
                    table += "<td colspan='5' class='text-center'>No data</td>";
                    table += "</tr>";
                }
                table += "</table>";
                $('#dataList').html(table);
            }
        });
    }
    else {
        GetAllMahasiswaMapped(0, 5);
    }
}

$("#searchBtn").click(function(){
    var keyword = $("#search").val();
    $("#searchTable").html(
        `
            <thead>
                <tr>>
                    <th>Nama Mahasiswa</th>
                    <th>Jenis Kelamin</th>
                    <th>Alamat</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody id="resultTBody"></tbody>
        `
    );

    $.ajax({
        url: "/api/search=" + keyword,
        type: "GET",
        contentType: "html",
        success: function(data){
            for(i = 0; i < data.length; i++){
                $("#resultTBody").append(
                    `
                        <tr>
                            <td>${data[i].namaMhs}</td>
                            <td>${data[i].jk}</td>
                            <td>${data[i].alamat}</td>
                            <td>
                                <button value="${data[i].id}" onClick="editMhs(${data[i].id})" class="btn btn-warning">
                                    <i class="bi-pencil-square"></i>
                                </button>
                                <button value="${data[i].id}" onClick="deleteMhs(${data[i].id})" class="btn btn-danger">
                                    <i class="bi-trash"></i>
                                </button>
                            </td>
                        </tr>
                    `
                )
            }
        }
    });
})

$(document).ready(function(){
//    GetAllMahasiswa(0, 5);
    GetAllMahasiswaMapped(0, 5);
})