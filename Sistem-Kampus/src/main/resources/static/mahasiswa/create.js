$("#addMhsBtnCancel").click(function(){
    $(".modal").modal("hide")
})

$("#addMhsBtnCreate").click(function(){
    var namaMhs = $("#namaMhs").val();
    var jk = $("#jk").val();
    var alamat = $("#alamat").val();

    if(namaMhs == ""){
        $("#errNamaMhs").text("Nama Mahasiswa tidak boleh kosong!");
        return;
    } else {
        $("#errNamaMhs").text("");
    }
    if(jk == ""){
        $("#errJK").text("Jenis Kelamin tidak boleh kosong!");
        return;
    } else {
        $("#errJK").text("");
    }
    if(alamat == ""){
        $("#alamat").text("Alamat tidak boleh kosong!");
        return;
    } else {
        $("#errAlamat").text("");
    }

    var object = {};
    object.namaMhs = namaMhs;
    object.jk = jk;
    object.alamat = alamat;

    var myJson = JSON.stringify(object);

    $.ajax({
        url: "/api/mahasiswa",
        type: "POST",
        contentType: "application/json",
        data: myJson,
        success: function(data){
            $(".modal").modal("hide")
            GetAllMahasiswa();
        },
        error: function(){
            alert("Gagal Menyimpan Data !!!!!")
        }
    });
})