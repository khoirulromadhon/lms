$("#addDosenBtnCancel").click(function(){
    $(".modal").modal("hide")
})

$("#addDosenBtnCreate").click(function(){
    var namaDosen = $("#namaDosen").val();
    var jk = $("#jk").val();
    var alamat = $("#alamat").val();

    if(namaDosen == ""){
        $("#errNamaDosen").text("Nama Dosen tidak boleh kosong!");
        return;
    } else {
        $("#errNamaDosen").text("");
    }
    if(jk == ""){
        $("#errJK").text("Jenis Kelamin tidak boleh kosong!");
        return;
    } else {
        $("#errJK").text("");
    }
    if(alamat == ""){
        $("#alamat").text("Alamat tidak boleh kosong!");
        return;
    } else {
        $("#errAlamat").text("");
    }

    var object = {};
    object.namaDosen = namaDosen;
    object.jk = jk;
    object.alamat = alamat;

    var myJson = JSON.stringify(object);

    $.ajax({
        url: "/api/dosen",
        type: "POST",
        contentType: "application/json",
        data: myJson,
        success: function(data){
            $(".modal").modal("hide")
            GetAllDosen();
        },
        error: function(){
            alert("Gagal Menyimpan Data !!!!!")
        }
    });
})