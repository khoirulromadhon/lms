$(document).ready(function(){
    GetDosenById();
})

function GetDosenById() {
    var id = $("#editDosenId").val();
    $.ajax({
        url: "/api/dosen/" + id,
        type: "GET",
        contentType: "application/json",
        success: function(data) {
            $("#namaDosen").val(data.namaDosen);
            $("#jk").val(data.jk);
            $("#alamat").val(data.alamat);
        }
    });
}

$("#updateDosenBtnCancel").click(function(){
    $(".modal").modal("hide")
})

$("#updateDosenBtnCreate").click(function(){
    var id = $("#editDosenId").val();
    var namaDosen = $("#namaDosen").val();
    var jk = $("#jk").val();
    var alamat = $("#alamat").val();

    if(namaDosen == ""){
        $("#errNamaDosen").text("Nama Dosen tidak boleh kosong!");
        return;
    } else {
        $("#errNamaDosen").text("");
    }
    if(jk == ""){
        $("#errJK").text("Jenis Kelamin tidak boleh kosong!");
        return;
    } else {
        $("#errJK").text("");
    }
    if(alamat == ""){
        $("#alamat").text("Alamat tidak boleh kosong!");
        return;
    } else {
        $("#errAlamat").text("");
    }

    var object = {}
    object.namaDosen = namaDosen;
    object.jk = jk;
    object.alamat = alamat;

    var myJson = JSON.stringify(object);

    $.ajax({
        url: "/api/dosen/" + id,
        type: "PUT",
        contentType: "application/json",
        data: myJson,
        success: function(data) {
            $(".modal").modal("hide")
            GetAllDosenMapped(0, 5);
        },
        error: function() {
            alert("Terjadi Kesalahan")
        }
    });
})