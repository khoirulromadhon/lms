function GetAllDosen(){
    $("#dosenTable").html(
        `
        <thead>
            <tr>>
                <th>Nama Dosen</th>
                <th>Jenis Kelamin</th>
                <th>Alamat</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody id="dosenTBody"></tbody>
        `
    );

    $.ajax({
        url: "api/dosen",
        type: "GET",
        contentType: "application/json",
        success: function(data){
            for(i = 0; i < data.length; i++){
                $("#dosenTBody").append(
                    `
                    <tr>
                        <td>${data[i].namaDosen}</td>
                        <td>${data[i].jk}</td>
                        <td>${data[i].alamat}</td>
                        <td>
                            <button value="${data[i].id}" onClick="editDosen(${data[i].id})" class="btn btn-warning">
                                <i class="bi-pencil-square"></i>
                            </button>
                            <button value="${data[i].id}" onClick="deleteDosen(${data[i].id})" class="btn btn-danger">
                                <i class="bi-trash"></i>
                            </button>
                        </td>
                    </tr>
                    `
                )
            }
        }
    });
}

$("#addBtn").click(function(){
    $.ajax({
        url: "/dosen/create",
        type: "GET",
        contentType: "html",
        success: function(data){
            $(".modal-title").text("Input Data Dosen Baru");
            $(".modal-body").html(data);
            $(".modal").modal("show");
        }
    });
})

function editDosen(id) {
    $.ajax({
        url: "/dosen/update/" +id,
        type: "GET",
        contentType: "html",
        success: function(data){
            $(".modal-title").text("Update Data Dosen");
            $(".modal-body").html(data);
            $(".modal").modal("show");
        }
    });
}

function deleteDosen(id){
    $.ajax({
        url: "/api/dosen/" + id,
        type: "DELETE",
        success: function(data) {
            location.reload();
            alert("Berhasil Menghapus Data !!!!!");
        },
        error: function() {
            alert("Gagal Menghapus Data !!!!!");
        }
    });
}

function GetAllDosenMapped(currentPage, length){
    $.ajax({
        url: "/api/dosenMapped?page=" + currentPage + "&size" + length,
        type: "GET",
        contentType: "application/json",
        success: function(data){
            let table = '<select class="custom-select mt-3" id="size" onchange="GetAllDosenMapped(0, this.value)">'
            table += '<option value="3" selected>..</option>'
            table += '<option value="5">5</option>'
            table += '<option value="10">10</option>'
            table += '<option value="15">15</option>'
            table += '</select>'
            table += "<table class='table table-bordered mt-3'>";
            table += "<tr> <th width='10%' class='text-center'>No</th> <th>Nama Dosen</th> <th>Jenis Kelamin</th> <th>Alamat</th>  <th>Action</th> </tr>"
            for (let i = 0; i < data.dosens.length; i++){
                table += "<tr>";
                table += "<td class='text-center'>" + ((i + 1) + (data.currentPage * length)) + "</td>";
                table += "<td>" + data.dosens[i].namaDosen + "</td>";
                table += "<td>" + data.dosens[i].jk + "</td>";
                table += "<td>" + data.dosens[i].alamat + "</td>";
                table += "<td><button class='btn btn-primary btn-sm' value='" + data.dosens[i].id + "' onclick=editDosen(this.value)>Edit</button> <button class='btn btn-danger btn-sm' value='" + data.dosens[i].id + "' onclick=deleteDosen(this.value)>Delete</button></td>";
                table += "</tr>";
            }
            table += "</table>";
            table += "<br>"
            table += '<nav aria-label="Page navigation">';
            table += '<ul class="pagination">'
            table += '<li class="page-item"><a class="page-link" onclick="GetAllDosenMapped(' + (data.currentPage - 1) + ',' + length + ')">Previous</a></li>'
            let index = 1;
            for (let i = 0; i < data.totalPages; i++){
                table += '<li class="page-item"><a id="pageslink" class="page-link" onclick="GetAllDosenMapped(' + i + ',' + length + ')">' + index + '</a></li>'
                index++;
            }
            table += '<li class="page-item"><a class="page-link" onclick="GetAllDosenMapped(' + (data.currentPage + 1) + ',' + length + ')">Next</a></li>'
            table += '</ul>'
            table += '</nav>';
            $('#dataList').html(table);
        }
    });
}

function Search(request) {
    if(request.length > 0){
        $.ajax({
            url: "/api/searchDosen=" + request,
            type: "GET",
            contentType: "application/json",
            success: function (result){
                let table = "<table class='table table-bordered mt-3'>"
                table += "<tr> <th width='10%' class='text-center'>No</th> <th>Nama Dosen</th> <th>Jenis Kelamin</th> <th>Alamat</th>  <th>Action</th> </tr>"
                if (result.length > 0){
                    for(let i = 0; i < result.length; i++){
                        table += "<tr>";
                        table += "<td class='text-center'>" + (i + 1) + "</td>";
                        table += "<td>" + result[i].namaDosen + "</td>";
                        table += "<td>" + result[i].jk + "</td>";
                        table += "<td>" + result[i].alamat + "</td>";
                        table += "<td><button class='btn btn-primary btn-sm' value='" + result[i].id + "' onclick=editDosen(this.value)>Edit</button> <button class='btn btn-danger btn-sm' value='" + result[i].id + "' onclick=deleteDosen(this.value)>Delete</button></td>";
                        table += "</tr>";
                    }
                }
                else {
                    table += "<tr>";
                    table += "<td colspan='5' class='text-center'>No data</td>";
                    table += "</tr>";
                }
                table += "</table>";
                $('#dataList').html(table);
            }
        });
    }
    else {
        GetAllDosenMapped(0, 5);
    }
}

$(document).ready(function(){
//    GetAllDosen();
    GetAllDosenMapped(0, 5);
})