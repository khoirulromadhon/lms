function GetAllDosen(){
    $.ajax({
        url: "/api/dosen",
        type: "GET",
        contentType: "application/json",
        success: function(data){
            for (i = 0; i < data.length; i++){
                $("#dosen").append(
                    `<option value="${data[i].id}">${data[i].namaDosen}</option>`
                )
            }
        }
    });
}

function GetAllKelas(){
    $.ajax({
        url: "/api/kelas",
        type: "GET",
        contentType: "application/json",
        success: function(data){
            for (i = 0; i < data.length; i++){
                $("#kelas").append(
                    `<option value="${data[i].id}">${data[i].namaKelas}</option>`
                )
            }
        }
    });
}

$("#addMatakuliahBtnCancel").click(function(){
    window.history.back();
})

$("#addMatakuliahBtnCreate").click(function(){
    var namaMk = $("#namaMatakuliah").val();
    var idKelas = $("#kelas").val();
    var idDosen = $("#dosen").val();

    if(namaMatakuliah == ""){
        $("#errNamaMatakuliah").text("Nama Matakuliah tidak boleh kosong!");
        return;
    } else {
        $("#errNamaMatakuliah").text("");
    }

    var object = {};

    object.namaMk = namaMk;
    object.idKelas = idKelas;
    object.idDosen = idDosen;

    var myJson = JSON.stringify(object);

    $.ajax({
        url: "/api/matakuliahHeader",
        type: "POST",
        contentType: "application/json",
        data: myJson,
        success: function(data){
            window.history.back();
            location.reload();
            alert("Berhasil Input Data !!!!!")
        },
        error: function(){
            alert("Tidak Dapat Menyimpan Data !!!!!")
        }
    });
})

$(document).ready(function(){
    GetAllDosen();
    GetAllKelas();
})