function GetMatakuliahHeader(currentPage, length){
    $.ajax({
        url: "/api/matakuliahHeader?page=" + currentPage + "&size" + length,
        type: "GET",
        contentType: "application/json",
        success: function(data){
            let table = '<select class="custom-select mt-3" id="size" onchange="GetMatakuliahHeader(0, 5)">'
            table += '<option value="3" selected>..</option>'
            table += '<option value="5">5</option>'
            table += '<option value="10">10</option>'
            table += '<option value="15">15</option>'
            table += '</select>'
            table += "<table class='table table-bordered mt-3'>";
            table += "<tr> <th width='10%' class='text-center'>No</th> <th>Nama Matakuliah</th> <th>Ruang</th> <th>Dosen</th>  <th>Action</th> </tr>"
            for (let i = 0; i < data.matakuliahList.length; i++){
                table += "<tr>";
                table += "<td class='text-center'>" + ((i + 1) + (data.currentPage * length)) + "</td>";
                table += "<td>" + data.matakuliahList[i].namaMk + "</td>";
                table += "<td>" + data.matakuliahList[i].kelas.namaKelas + "</td>";
                table += "<td>" + data.matakuliahList[i].dosen.namaDosen + "</td>";
                table += "<td><button class='btn btn-primary btn-sm' value='" + data.matakuliahList[i].id + "' onclick=mkDetail(this.value)>View Details</button>'";
                table += "</tr>";
            }
            table += "</table>";
            table += "<br>"
            table += '<nav aria-label="Page navigation">';
            table += '<ul class="pagination">'
            table += '<li class="page-item"><a class="page-link" onclick="GetMatakuliahHeader(' + (data.currentPage - 1) + ',' + length + ')">Previous</a></li>'
            let index = 1;
            for (let i = 0; i < data.totalPages; i++){
                table += '<li class="page-item"><a id="pageslink" class="page-link" onclick="GetMatakuliahHeader(' + i + ',' + length + ')">' + index + '</a></li>'
                index++;
            }
            table += '<li class="page-item"><a class="page-link" onclick="GetMatakuliahHeader(' + (data.currentPage + 1) + ',' + length + ')">Next</a></li>'
            table += '</ul>'
            table += '</nav>';
            $('#matakuliahList').html(table);
        }
    });
}

function mkDetail(id){
    $.ajax({
        url: "/matakuliah/detail",
        type: "GET",
        contentType: "html",
        success: function(data){
            var idMatakuliahHeader = id;
            localStorage.setItem('idMatakuliahHeaderDetails', idMatakuliahHeader);
            window.location.href = "/matakuliah/detail";
        }
    });
}

$("#addBtn").click(function(){
    $.ajax({
        url: "/matakuliah/create",
        type: "GET",
        contentType: "html",
        success: function(data){
            window.location.href = "/matakuliah/create";
        }
    });
})

$(document).ready(function(){
    GetMatakuliahHeader(0, 5);
})