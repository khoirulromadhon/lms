function GetAllMhs(){
    $.ajax({
        url: "/api/mahasiswa",
        type: "GET",
        contentType: "application/json",
        success: function(data){
            for (i = 0; i < data.length; i++){
                $("#mahasiswa").append(
                    `<option value="${data[i].id}">${data[i].namaMhs}</option>`
                )
            }
        }
    });
}

function GetMatakuliah(){
    var id = localStorage.getItem('idMatakuliahHeader');
    $.ajax({
        url: "/api/matakuliahHeader/" + id,
        type: "GET",
        contentType: "application/json",
        success: function(data){
            $("#namaMk").val(data.namaMk);
        }
    });
}

$("#addMhsBtnCancel").click(function(){
    window.history.back();
})

$("#addMhsBtnCreate").click(function(){
    var idMatakuliah = localStorage.getItem('idMatakuliahHeader');
    var idMhs = $("#mahasiswa").val();

    var object = {};

    object.idMatakuliah = idMatakuliah;
    object.idMhs = idMhs;

    var myJson = JSON.stringify(object);

    $.ajax({
        url: "/api/matakuliahDetails",
        type: "POST",
        contentType: "application/json",
        data: myJson,
        success: function(data){
            window.history.back();
            location.reload();
            alert("Berhasil Input Data !!!!!")
        },
        error: function(){
            alert("Tidak Dapat Menyimpan Data !!!!!")
        }
    });
})

$(document).ready(function(){
    GetAllMhs();
    GetMatakuliah();
})