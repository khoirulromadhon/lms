function GetAllMahasiswa(){
    var idMatakuliahHeaderDetails = localStorage.getItem('idMatakuliahHeaderDetails');
    $("#mahasiswaTable").html(
        `
            <thead>
                <tr>
                    <th>Nama Mahasiswa</th>
                    <th>Jenis Kelamin</th>
                    <th>Alamat</th>
                </tr>
            </thead>
            <tbody id="mahasiswaTBody"></tbody>
        `
    );

    $.ajax({
        url: "/api/matakuliahDetails/" + idMatakuliahHeaderDetails,
        type: "GET",
        contentType: "application/json",
        success: function(data){
            for(i = 0; i < data.length; i++){
                $("#mahasiswaTBody").append(
                    `
                        <tr>
                            <td>${data[i].mahasiswa.namaMhs}</td>
                            <td>${data[i].mahasiswa.jk}</td>
                            <td>${data[i].mahasiswa.alamat}</td>
                        </tr>
                    `
                )
            }
        }
    });
}

$("#addBtn").click(function(){
    var idMatakuliahHeaderDetails = localStorage.getItem('idMatakuliahHeaderDetails');
    $.ajax({
        url: "/matakuliah/detailCreate",
        type: "GET",
        contentType: "html",
        success: function(data){
            var idMatakuliahHeader = idMatakuliahHeaderDetails;
            localStorage.setItem('idMatakuliahHeader', idMatakuliahHeader);
            window.location.href = "/matakuliah/detailCreate";
        }
    });
})

$(document).ready(function(){
    GetAllMahasiswa();
})

//localStorage.removeItem('idMatakuliahHeaderDetails');