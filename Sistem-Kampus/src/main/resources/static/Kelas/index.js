function GetAllKelas(){
    $("#kelasTable").html(
        `
        <thead>
            <tr>>
                <th>Id Kelas</th>
                <th>Nama Kelas</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody id="kelasTBody"></tbody>
        `
    );

    $.ajax({
        url: "api/kelas",
        type: "GET",
        contentType: "application/json",
        success: function(data){
            for(i = 0; i < data.length; i++){
                $("#kelasTBody").append(
                    `
                    <tr>
                        <td>${data[i].id}</td>
                        <td>${data[i].namaKelas}</td>
                        <td>
                            <button value="${data[i].id}" onClick="editKelas(${data[i].id})" class="btn btn-warning">
                                <i class="bi-pencil-square"></i>
                            </button>
                            <button value="${data[i].id}" onClick="deleteKelas(${data[i].id})" class="btn btn-danger">
                                <i class="bi-trash"></i>
                            </button>
                        </td>
                    </tr>
                    `
                )
            }
        }
    });
}

$("#addBtn").click(function(){
    $.ajax({
        url: "/kelas/create",
        type: "GET",
        contentType: "html",
        success: function(data){
            $(".modal-title").text("Input Data Kelas Baru");
            $(".modal-body").html(data);
            $(".modal").modal("show");
        }
    });
})

function editKelas(id) {
    $.ajax({
        url: "/kelas/update/" + id,
        type: "GET",
        contentType: "html",
        success: function(data) {
            $(".modal-title").text("Update Data Kelas");
            $(".modal-body").html(data);
            $(".modal").modal("show");
        }
    });
}

function deleteKelas(id){
    $.ajax({
        url: "/api/kelas/" + id,
        type: "DELETE",
        success: function(data){
            location.reload();
            alert("Berhasil Menghapus Data !!!!!");
        },
        error: function() {
            alert("Gagal Menghapus Data !!!!!");
        }
    });
}

$(document).ready(function(){
    GetAllKelas();
})