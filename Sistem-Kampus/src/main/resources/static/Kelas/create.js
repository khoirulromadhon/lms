$("#addKelasnBtnCancel").click(function(){
    $(".modal").modal("hide")
})

$("#addKelasBtnCreate").click(function(){
    var namaKelas = $("#namaKelas").val();

     if(namaKelas == ""){
        $("#errNamaKelas").text("Nama Kelas tidak boleh kosong!");
        return;
     }
     else {
        $("#errNamaKelas").text("");
     }

     var object = {}
     object.namaKelas = namaKelas;

     var myJson = JSON.stringify(object);

     $.ajax({
        url: "/api/kelas",
        type: "POST",
        contentType: "application/json",
        data: myJson,
        success: function(data){
            $(".modal").modal("hide")
            GetAllKelas();
        },
        error: function(){
            alert("Gagal Menyimpan Data !!!!!")
        }
     });
})