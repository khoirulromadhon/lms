$(document).ready(function(){
    GetKelasById();
})

function GetKelasById() {
    var id = $("#editKelasId").val();
    $.ajax({
        url: "/api/kelas/" + id,
        type: "GET",
        contentType: "application/json",
        success: function(data) {
            $("#namaKelas").val(data.namaKelas);
        }
    });
}

$("#updateKelasnBtnCancel").click(function(){
    $(".modal").modal("hide")
})

$("#updateKelasBtnCreate").click(function(){
    var id = $("#editKelasId").val();
    var namaKelas = $("#namaKelas").val();

     if(namaKelas == ""){
        $("#errNamaKelas").text("Nama Kelas tidak boleh kosong!");
        return;
     }
     else {
        $("#errNamaKelas").text("");
     }

     var object = {}
     object.namaKelas = namaKelas;

     var myJson = JSON.stringify(object);

     $.ajax({
        url: "/api/kelas/" + id,
        type: "PUT",
        contentType: "application/json",
        data: myJson,
        success: function(data){
            $(".modal").modal("hide")
            GetAllKelas();
        },
        error: function(){
            alert("Gagal Menyimpan Data !!!!!")
        }
     });
})