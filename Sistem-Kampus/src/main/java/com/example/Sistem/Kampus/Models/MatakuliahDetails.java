package com.example.Sistem.Kampus.Models;

import javax.persistence.*;

@Entity
@Table(name = "matakuliah_detail")
public class MatakuliahDetails extends CommonEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id", nullable = false)
    private Long Id;

    @ManyToOne
    @JoinColumn(name = "id_matakuliah", insertable = false, updatable = false)
    public Matakuliah matakuliah;

    @Column(name = "id_matakuliah", nullable = true)
    private long idMatakuliah;

    @ManyToOne
    @JoinColumn(name = "id_mhs", insertable = false, updatable = false)
    public Mahasiswa mahasiswa;

    @Column(name = "id_mhs", nullable = true)
    private long idMhs;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public Matakuliah getMatakuliah() {
        return matakuliah;
    }

    public void setMatakuliah(Matakuliah matakuliah) {
        this.matakuliah = matakuliah;
    }

    public long getIdMatakuliah() {
        return idMatakuliah;
    }

    public void setIdMatakuliah(long idMatakuliah) {
        this.idMatakuliah = idMatakuliah;
    }

    public Mahasiswa getMahasiswa() {
        return mahasiswa;
    }

    public void setMahasiswa(Mahasiswa mahasiswa) {
        this.mahasiswa = mahasiswa;
    }

    public long getIdMhs() {
        return idMhs;
    }

    public void setIdMhs(long idMhs) {
        this.idMhs = idMhs;
    }
}
