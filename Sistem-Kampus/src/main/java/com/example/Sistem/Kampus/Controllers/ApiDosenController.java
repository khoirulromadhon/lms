package com.example.Sistem.Kampus.Controllers;

import com.example.Sistem.Kampus.Models.Dosen;
import com.example.Sistem.Kampus.Repositories.DosenRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiDosenController {
    @Autowired
    private DosenRepositories dosenRepositories;

    @GetMapping("/dosen")
    public ResponseEntity<List<Dosen>> GetAllDosen(){
        try {
            List<Dosen> dosenList = this.dosenRepositories.findAll();
            return new ResponseEntity<>(dosenList, HttpStatus.OK);
        }
        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/dosen")
    public ResponseEntity<Object> SaveDosen(@RequestBody Dosen dosen){
        try {
            dosen.setCreatedBy("Khoirul");
            dosen.setCreatedOn(new Date());
            this.dosenRepositories.save(dosen);
            return new ResponseEntity<>(dosen, HttpStatus.OK);
        }
        catch (Exception exception) {
            return new ResponseEntity<>("Fail to save data !!!!!", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/dosen/{id}")
    public ResponseEntity<List<Dosen>> GetDosenById(@PathVariable("id") Long id) {
        try {
            Optional<Dosen> dosen = this.dosenRepositories.findById(id);

            if (dosen.isPresent()){
                ResponseEntity response = new ResponseEntity<>(dosen, HttpStatus.OK);
                return response;
            }
            else {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/dosen/{id}")
    public ResponseEntity<Object> UpdateDosen(@RequestBody Dosen dosen, @PathVariable("id") Long id){
        Optional<Dosen> dosenData = this.dosenRepositories.findById(id);
        dosen.setCreatedBy("Khoirul");
        dosen.setCreatedOn(new Date());
        dosen.setModifiedBy("Khoirul");
        dosen.setModifiedOn(new Date());

        if (dosenData.isPresent()) {
            dosen.setId(id);
            this.dosenRepositories.save(dosen);
            ResponseEntity response = new ResponseEntity<>("Success Update Data !!!!!", HttpStatus.OK);
            return response;
        }
        else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/dosen/{id}")
    public ResponseEntity<Object> DeleteDosen(@PathVariable("id") Long id){
        this.dosenRepositories.deleteById(id);
        return new ResponseEntity<>("Sukses Menghapus Data !!!!!", HttpStatus.OK);
    }

    @GetMapping("/dosenMapped")
    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size){
        try {
            List<Dosen> dosens = new ArrayList<>();
            Pageable paging = PageRequest.of(page, size, Sort.by("namaDosen").ascending());

            Page<Dosen> pageTuts;

            pageTuts = dosenRepositories.findAll(paging);

            dosens = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("dosens", dosens);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/searchDosen={keyword}")
    public ResponseEntity<List<Dosen>> searchDosen(@PathVariable("keyword") String keyword){
        try{
            List<Dosen> dosenList = this.dosenRepositories.searchDosen(keyword);
            return  new ResponseEntity<>(dosenList, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
