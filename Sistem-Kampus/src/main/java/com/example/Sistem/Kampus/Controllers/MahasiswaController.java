package com.example.Sistem.Kampus.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "mahasiswa")
public class MahasiswaController {
    @RequestMapping("")
    public String index() {
        return "mahasiswa/index";
    }

    @RequestMapping("/create")
    public String createMahasiswa() {
        return "mahasiswa/create";
    }

    @RequestMapping("/update/{id}")
    public String updateMahasiswa(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "mahasiswa/update";
    }
}
