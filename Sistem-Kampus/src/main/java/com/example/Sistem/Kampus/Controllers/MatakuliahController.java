package com.example.Sistem.Kampus.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "matakuliah")
public class MatakuliahController {
    @RequestMapping("")
    public String index() {
        return "Matakuliah/index";
    }

    @RequestMapping("/detail")
    public String detail() {
        return "Matakuliah/details";
    }

    @RequestMapping("/create")
    public String create(){
        return "Matakuliah/createHeader";
    }

    @RequestMapping("/detailCreate")
    public String detailCreate(){
        return "Matakuliah/createDetails";
    }
}
