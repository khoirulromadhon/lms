package com.example.Sistem.Kampus.Controllers;

import com.example.Sistem.Kampus.Models.Mahasiswa;
import com.example.Sistem.Kampus.Repositories.MahasiswaRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiMahasiswaController {
    @Autowired
    private MahasiswaRepositories mahasiswaRepositories;

    @GetMapping("/mahasiswa")
    public ResponseEntity<List<Mahasiswa>> GetAllMahasiswa(){
        try {
            List<Mahasiswa> mahasiswaList = this.mahasiswaRepositories.findAll();
            return new ResponseEntity<>(mahasiswaList, HttpStatus.OK);
        }
        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/mahasiswa")
    public ResponseEntity<Object> SaveMahasiswa(@RequestBody Mahasiswa mahasiswa) {
        try {
            mahasiswa.setCreatedBy("Khoirul");
            mahasiswa.setCreatedOn(new Date());
            this.mahasiswaRepositories.save(mahasiswa);
            return new ResponseEntity<>(mahasiswa, HttpStatus.OK);
        }
        catch (Exception exception) {
            return new ResponseEntity<>("Fail To Save Data !!!!!", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/mahasiswa/{id}")
    public ResponseEntity<List<Mahasiswa>> GetMahasiswaById(@PathVariable("id") Long id) {
        try {
            Optional<Mahasiswa> mahasiswa = this.mahasiswaRepositories.findById(id);

            if (mahasiswa.isPresent()) {
                ResponseEntity response = new ResponseEntity<>(mahasiswa, HttpStatus.OK);
                return response;
            }
            else {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/mahasiswa/{id}")
    public ResponseEntity<Object> UpdateMahasiswa(@RequestBody Mahasiswa mahasiswa, @PathVariable("id") Long id) {
        Optional<Mahasiswa> mahasiswaData = this.mahasiswaRepositories.findById(id);
        mahasiswa.setCreatedBy("Khoirul");
        mahasiswa.setCreatedOn(new Date());
        mahasiswa.setModifiedBy("Khoirul");
        mahasiswa.setModifiedOn(new Date());

        if (mahasiswaData.isPresent()){
            mahasiswa.setId(id);
            this.mahasiswaRepositories.save(mahasiswa);
            ResponseEntity response = new ResponseEntity<>("Success Update Data !!!!!!", HttpStatus.OK);
            return response;
        }
        else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/mahasiswa/{id}")
    public ResponseEntity<Object> DeleteMahasiswa(@PathVariable("id") Long id) {
        this.mahasiswaRepositories.deleteById(id);
        return new ResponseEntity<>("Sukses Menghapus Data !!!!!", HttpStatus.OK);
    }

    @GetMapping("/mahasiswaMapped")
    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size){
        try {
            List<Mahasiswa> mahasiswas = new ArrayList<>();
            Pageable paging = PageRequest.of(page, size, Sort.by("namaMhs").ascending());

            Page<Mahasiswa> pageTuts;

            pageTuts = mahasiswaRepositories.findAll(paging);

            mahasiswas = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("mahasiswas", mahasiswas);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/searchMhs={keyword}")
    public ResponseEntity<List<Mahasiswa>> searchMahasiwa(@PathVariable("keyword") String keyword){
        try{
            List<Mahasiswa> mahasiswaList = this.mahasiswaRepositories.searchMahasiswa(keyword);
            return new ResponseEntity<>(mahasiswaList, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
