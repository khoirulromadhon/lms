package com.example.Sistem.Kampus.Repositories;

import com.example.Sistem.Kampus.Models.MatakuliahDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MatakuliahDetailsReposiroties extends JpaRepository<MatakuliahDetails, Long> {
    @Query("FROM MatakuliahDetails WHERE idMatakuliah = ?1")
    List<MatakuliahDetails> findByMatakuliahId(Long idMatakuliah);
}
