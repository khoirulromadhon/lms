package com.example.Sistem.Kampus.Models;

import javax.persistence.*;

@Entity
@Table(name = "mahasiswa")
public class Mahasiswa extends CommonEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id", nullable = false)
    private Long Id;

    @Column(name = "nama_mhs", nullable = false)
    private String namaMhs;

    @Column(name = "jk", nullable = false)
    private String jk;

    @Column(name = "alamat", nullable = false)
    private String alamat;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getNamaMhs() {
        return namaMhs;
    }

    public void setNamaMhs(String namaMhs) {
        this.namaMhs = namaMhs;
    }

    public String getJk() {
        return jk;
    }

    public void setJk(String jk) {
        this.jk = jk;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }
}
