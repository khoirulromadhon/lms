package com.example.Sistem.Kampus.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DefaultController {
    @RequestMapping("")
    public String dashboard() {
        return "dashboard";
    }
}
