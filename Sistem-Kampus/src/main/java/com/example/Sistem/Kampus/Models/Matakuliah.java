package com.example.Sistem.Kampus.Models;

import javax.persistence.*;

@Entity
@Table(name = "matakuliah")
public class Matakuliah extends CommonEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id", nullable = false)
    private Long Id;

    @Column(name = "nama_mk", nullable = false)
    private String namaMk;

    @ManyToOne
    @JoinColumn(name = "id_kelas", insertable = false, updatable = false)
    public Kelas kelas;

    @Column(name = "id_kelas", nullable = true)
    private Long idKelas;

    @ManyToOne
    @JoinColumn(name = "id_dosen", insertable = false, updatable = false)
    public Dosen dosen;

    @Column(name = "id_dosen", nullable = true)
    private Long idDosen;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getNamaMk() {
        return namaMk;
    }

    public void setNamaMk(String namaMk) {
        this.namaMk = namaMk;
    }

    public Kelas getKelas() {
        return kelas;
    }

    public void setKelas(Kelas kelas) {
        this.kelas = kelas;
    }

    public Long getIdKelas() {
        return idKelas;
    }

    public void setIdKelas(Long idKelas) {
        this.idKelas = idKelas;
    }

    public Dosen getDosen() {
        return dosen;
    }

    public void setDosen(Dosen dosen) {
        this.dosen = dosen;
    }

    public Long getIdDosen() {
        return idDosen;
    }

    public void setIdDosen(Long idDosen) {
        this.idDosen = idDosen;
    }
}
