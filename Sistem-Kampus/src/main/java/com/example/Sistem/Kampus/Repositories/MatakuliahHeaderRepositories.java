package com.example.Sistem.Kampus.Repositories;

import com.example.Sistem.Kampus.Models.Matakuliah;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MatakuliahHeaderRepositories extends JpaRepository<Matakuliah, Long> {
}
