package com.example.Sistem.Kampus.Controllers;

import com.example.Sistem.Kampus.Models.Matakuliah;
import com.example.Sistem.Kampus.Repositories.MatakuliahHeaderRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiMatakuliahHeaderController {
    @Autowired
    private MatakuliahHeaderRepositories matakuliahHeaderRepositories;

    @GetMapping("/matakuliahHeader")
    public ResponseEntity<Map<String, Object>> GetAllMatakuliahHeader(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size){
        try {
            List<Matakuliah> matakuliahList = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);

            Page<Matakuliah> pageTuts;

            pageTuts = matakuliahHeaderRepositories.findAll(pagingSort);

            matakuliahList = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("matakuliahList", matakuliahList);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/matakuliahHeader/{id}")
    public ResponseEntity<List<Matakuliah>> GetMatakuliahHeaderById(@PathVariable("id") Long id){
        try{
            Optional<Matakuliah> matakuliahHeader = this.matakuliahHeaderRepositories.findById(id);

            if (matakuliahHeader.isPresent()){
                ResponseEntity response = new ResponseEntity<>(matakuliahHeader, HttpStatus.OK);
                return response;
            }
            else {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping(value = "/matakuliahHeader")
    public ResponseEntity<Object> SaveMatakuliahHeader(@RequestBody Matakuliah matakuliah){
        try{
            matakuliah.setCreatedBy("Khoirul");
            matakuliah.setCreatedOn(new Date());
            this.matakuliahHeaderRepositories.save(matakuliah);
            return new ResponseEntity<>("Suksess Input Data !!!!!", HttpStatus.CREATED);
        }
        catch (Exception exception){
            return new ResponseEntity<>("Fail Input Data !!!!!", HttpStatus.BAD_REQUEST);
        }
    }
}
