package com.example.Sistem.Kampus.Repositories;

import com.example.Sistem.Kampus.Models.Kelas;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KelasRepositories extends JpaRepository<Kelas, Long> {
}
