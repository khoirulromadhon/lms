package com.example.Sistem.Kampus.Repositories;

import com.example.Sistem.Kampus.Models.Dosen;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DosenRepositories extends JpaRepository<Dosen, Long> {
    @Query(value = "SELECT * FROM dosen WHERE lower(nama_Dosen) LIKE lower (concat('%', :keyword, '%'))", nativeQuery = true)
    List<Dosen> searchDosen(String keyword);
}
