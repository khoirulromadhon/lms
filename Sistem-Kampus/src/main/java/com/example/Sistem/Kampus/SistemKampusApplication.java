package com.example.Sistem.Kampus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SistemKampusApplication {

	public static void main(String[] args) {
		SpringApplication.run(SistemKampusApplication.class, args);
	}

}
