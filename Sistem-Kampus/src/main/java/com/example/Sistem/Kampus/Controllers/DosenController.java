package com.example.Sistem.Kampus.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "dosen")
public class DosenController {
    @RequestMapping("")
    public String index() {
        return "dosen/index";
    }

    @RequestMapping("/create")
    public String createDosen(){
        return "dosen/create";
    }

    @RequestMapping("/update/{id}")
    public String editDosen(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "dosen/update";
    }
}
