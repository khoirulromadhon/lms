package com.example.Sistem.Kampus.Models;


import javax.persistence.*;

@Entity
@Table(name = "nilai_detail")
public class NilaiDetails extends CommonEntity{

    @Id@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long Id;

    @ManyToOne
    @JoinColumn(name = "id_nilai_header", insertable = false, updatable = false)
    public NilaiHeader nilaiHeader;

    @Column(name = "id_nilai_header", nullable = true)
    private Long idNilaiHeader;

    @ManyToOne
    @JoinColumn(name = "id_mk", insertable = false, updatable = false)
    public Matakuliah matakuliah;

    @Column(name = "id_mk", nullable = true)
    private Long idMk;

    @Column(name = "nilai", nullable = true)
    private Double nilai;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public NilaiHeader getNilaiHeader() {
        return nilaiHeader;
    }

    public void setNilaiHeader(NilaiHeader nilaiHeader) {
        this.nilaiHeader = nilaiHeader;
    }

    public Long getIdNilaiHeader() {
        return idNilaiHeader;
    }

    public void setIdNilaiHeader(Long idNilaiHeader) {
        this.idNilaiHeader = idNilaiHeader;
    }

    public Matakuliah getMatakuliah() {
        return matakuliah;
    }

    public void setMatakuliah(Matakuliah matakuliah) {
        this.matakuliah = matakuliah;
    }

    public Long getIdMk() {
        return idMk;
    }

    public void setIdMk(Long idMk) {
        this.idMk = idMk;
    }

    public Double getNilai() {
        return nilai;
    }

    public void setNilai(Double nilai) {
        this.nilai = nilai;
    }
}
