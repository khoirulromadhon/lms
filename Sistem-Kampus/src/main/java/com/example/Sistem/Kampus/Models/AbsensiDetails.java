package com.example.Sistem.Kampus.Models;

import javax.persistence.*;

@Entity
@Table(name = "absensi_detail")
public class AbsensiDetails extends CommonEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id", nullable = false)
    private Long Id;

    @ManyToOne
    @JoinColumn(name = "id_absensi_header", insertable = false, updatable = false)
    public  AbsensiHeader absensiHeader;

    @Column(name = "id_absensi_header", nullable = true)
    private Long idAbsensiHeader;

    @Column(name = "A", nullable = true)
    private int a;

    @Column(name = "I", nullable = true)
    private  int i;

    @Column(name = "S", nullable = true)
    private int s;

    @Column(name = "H", nullable = true)
    private int h;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public AbsensiHeader getAbsensiHeader() {
        return absensiHeader;
    }

    public void setAbsensiHeader(AbsensiHeader absensiHeader) {
        this.absensiHeader = absensiHeader;
    }

    public Long getIdAbsensiHeader() {
        return idAbsensiHeader;
    }

    public void setIdAbsensiHeader(Long idAbsensiHeader) {
        this.idAbsensiHeader = idAbsensiHeader;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    public int getS() {
        return s;
    }

    public void setS(int s) {
        this.s = s;
    }

    public int getH() {
        return h;
    }

    public void setH(int h) {
        this.h = h;
    }
}
