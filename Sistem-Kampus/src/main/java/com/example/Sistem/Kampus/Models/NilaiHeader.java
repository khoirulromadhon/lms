package com.example.Sistem.Kampus.Models;


import javax.persistence.*;

@Entity
@Table(name = "nilai_header")
public class NilaiHeader extends CommonEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long Id;

    @ManyToOne
    @JoinColumn(name = "id_mhs", insertable = false, updatable = false)
    public Mahasiswa mahasiswa;

    @Column(name = "id_mhs", nullable = true)
    private Long idMhs;

    @Column(name = "rata_nilai", nullable = true)
    private Double rataNilai;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public Mahasiswa getMahasiswa() {
        return mahasiswa;
    }

    public void setMahasiswa(Mahasiswa mahasiswa) {
        this.mahasiswa = mahasiswa;
    }

    public Long getIdMhs() {
        return idMhs;
    }

    public void setIdMhs(Long idMhs) {
        this.idMhs = idMhs;
    }

    public Double getRataNilai() {
        return rataNilai;
    }

    public void setRataNilai(Double rataNilai) {
        this.rataNilai = rataNilai;
    }
}
