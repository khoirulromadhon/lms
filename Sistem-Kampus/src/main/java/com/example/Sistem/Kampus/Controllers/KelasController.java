package com.example.Sistem.Kampus.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "kelas")
public class KelasController {
    @RequestMapping("")
    public String index() {
        return "kelas/index";
    }

    @RequestMapping("/create")
    public String createKelas() {
        return "kelas/create";
    }

    @RequestMapping("/update/{id}")
    public String updateKelas(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "kelas/update";
    }
}
