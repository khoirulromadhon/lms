package com.example.Sistem.Kampus.Controllers;

import com.example.Sistem.Kampus.Models.Kelas;
import com.example.Sistem.Kampus.Repositories.KelasRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiKelasController {
    @Autowired
    private KelasRepositories kelasRepositories;

    @GetMapping("/kelas")
    public ResponseEntity<List<Kelas>> GetAllKelas(){
        try {
            List<Kelas> kelasList = this.kelasRepositories.findAll();
            return new ResponseEntity<>(kelasList, HttpStatus.OK);
        }
        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/kelas")
    public ResponseEntity<Object> SaveKelas(@RequestBody Kelas kelas) {
        try {
            kelas.setCreatedBy("Khoirul");
            kelas.setCreatedOn(new Date());
            this.kelasRepositories.save(kelas);
            return new ResponseEntity<>(kelas, HttpStatus.OK);
        }
        catch (Exception exception) {
            return new ResponseEntity<>("Fail to save data !!!!!", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/kelas/{id}")
    public ResponseEntity<List<Kelas>> GetKelasById(@PathVariable("id") Long id) {
        try {
            Optional<Kelas> kelas = this.kelasRepositories.findById(id);

            if (kelas.isPresent()){
                ResponseEntity response = new ResponseEntity<>(kelas, HttpStatus.OK);
                return response;
            }
            else {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/kelas/{id}")
    public ResponseEntity<Object> UpdateKelas(@RequestBody Kelas kelas, @PathVariable("id") Long id) {
        Optional<Kelas> kelasData = this.kelasRepositories.findById(id);
        kelas.setCreatedBy("Khoirul");
        kelas.setCreatedOn(new Date());
        kelas.setModifiedBy("Khoirul");
        kelas.setModifiedOn(new Date());

        if (kelasData.isPresent()) {
            kelas.setId(id);
            this.kelasRepositories.save(kelas);
            ResponseEntity response = new ResponseEntity<>("Success Update Data !!!!!", HttpStatus.OK);
            return response;
        }
        else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/kelas/{id}")
    public ResponseEntity<Object> DeleteKelas(@PathVariable("id") Long id) {
        this.kelasRepositories.deleteById(id);
        return new ResponseEntity<>("Suksess Menghapus Data !!!!!", HttpStatus.OK);
    }
}
