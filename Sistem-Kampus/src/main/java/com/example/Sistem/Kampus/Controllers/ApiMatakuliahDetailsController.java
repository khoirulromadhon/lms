package com.example.Sistem.Kampus.Controllers;

import com.example.Sistem.Kampus.Models.MatakuliahDetails;
import com.example.Sistem.Kampus.Repositories.MatakuliahDetailsReposiroties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiMatakuliahDetailsController {
    @Autowired
    private MatakuliahDetailsReposiroties matakuliahDetailsReposiroties;

    @GetMapping("/matakuliahDetails")
    public ResponseEntity<List<MatakuliahDetails>> GetAllMatakuliahDetail(){
        try{
            List<MatakuliahDetails> matakuliahDetailsList = this.matakuliahDetailsReposiroties.findAll();
            return new ResponseEntity<>(matakuliahDetailsList, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/matakuliahDetails/{id}")
    public ResponseEntity<List<MatakuliahDetails>> GetMatakuliahById(@PathVariable("id") Long id){
        try {
            List<MatakuliahDetails> matakuliahDetails = this.matakuliahDetailsReposiroties.findByMatakuliahId(id);
            return new ResponseEntity<>(matakuliahDetails, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/matakuliahDetails")
    public ResponseEntity<Object> SaveMatakuliahDetail(@RequestBody MatakuliahDetails matakuliahDetails){
        try {
            matakuliahDetails.setCreatedBy("Khoirul");
            matakuliahDetails.setCreatedOn(new Date());
            this.matakuliahDetailsReposiroties.save(matakuliahDetails);
            return new ResponseEntity<>("Sukses Input Data !!!!!", HttpStatus.CREATED);
        }
        catch (Exception exception){
            return new ResponseEntity<>("Failed Input Data !!!!!", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/matakuliahDetails/{id}")
    public ResponseEntity<Object> UpdateMatakuliahDetails(@RequestBody MatakuliahDetails matakuliahDetails, @PathVariable("id") Long id){
        Optional<MatakuliahDetails> matakuliahDetailsData = this.matakuliahDetailsReposiroties.findById(id);

        if (matakuliahDetailsData.isPresent()){
            matakuliahDetails.setModifiedBy("Khoirul");
            matakuliahDetails.setCreatedBy("Khoirul");
            matakuliahDetails.setCreatedOn(new Date());
            matakuliahDetails.setModifiedOn(new Date());
            matakuliahDetails.setId(id);
            this.matakuliahDetailsReposiroties.save(matakuliahDetails);
            ResponseEntity response = new ResponseEntity<>("Sukses Update Data !!!!!", HttpStatus.CREATED);
            return response;
        }
        else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/matakuliahDetails/{id}")
    public ResponseEntity<Object> DeleteMatakuliahDetail(@PathVariable("id") Long id){
        this.matakuliahDetailsReposiroties.deleteById(id);
        return new ResponseEntity<>("Suksess Delete Data !!!!!", HttpStatus.OK);
    }
}
