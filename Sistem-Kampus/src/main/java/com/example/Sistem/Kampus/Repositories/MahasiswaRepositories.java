package com.example.Sistem.Kampus.Repositories;

import com.example.Sistem.Kampus.Models.Mahasiswa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface MahasiswaRepositories extends JpaRepository<Mahasiswa, Long> {
    @Query(value = "SELECT * FROM mahasiswa WHERE lower(nama_Mhs) LIKE lower (concat('%', :keyword, '%'))", nativeQuery = true)
    List<Mahasiswa> searchMahasiswa(String keyword);
}
